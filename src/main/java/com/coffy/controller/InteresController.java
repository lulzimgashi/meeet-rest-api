package com.coffy.controller;

import com.coffy.model.Interes;
import com.coffy.service.InteresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 12/02/2018.
 */
@RestController
@RequestMapping("interests")
public class InteresController {

    @Autowired
    private InteresService interesService;


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Interes> addInteres(@RequestBody Interes interes) {
        Interes savedInteres = interesService.addInteres(interes);

        return new ResponseEntity<>(savedInteres, HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteInteres(@PathVariable String id) {
        String response = interesService.deleteInteres(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
