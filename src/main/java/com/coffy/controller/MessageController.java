package com.coffy.controller;

import com.coffy.model.Message;
import com.coffy.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by lulzimgashi on 17/02/2018.
 */
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @MessageMapping("/messages/{chatId}")
    public void sendMessage(@DestinationVariable("chatId") String chatId, Message message,Principal principal) {
        messageService.sendMessage(chatId, message);
    }
}
