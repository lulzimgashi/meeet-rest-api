package com.coffy.controller;

import com.coffy.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by lulzimgashi on 12/02/2018.
 */
@RestController
@RequestMapping("documents")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<String> addImage(@RequestParam("file") MultipartFile file) throws IOException {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        file.getInputStream();

        String url = imageService.upload(file.getInputStream(), currentUser);
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

}
