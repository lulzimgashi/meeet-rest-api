package com.coffy.controller;

import com.coffy.exception.EmailExistExeption;
import com.coffy.exception.UserExistException;
import com.coffy.model.User;
import com.coffy.service.UserService;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<User> addUser(@RequestBody User user) throws UserExistException {
        User savedUser = userService.addUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@RequestBody User user) throws EmailExistExeption {
        User savedUser = userService.updateUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "password", method = RequestMethod.PUT)
    public ResponseEntity<String> changePassword(@RequestParam("type") String type, @RequestParam("value") String value) {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        if (type.equals("change")) {
            String response = userService.changePassword(currentUser, value);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return null;
        }
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ResponseEntity<User> getLoggedUser() throws EmailExistExeption {

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getLoggedUser(currentUser);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers(@RequestParam("keyword") String keyword,
                                               @RequestParam("id") String id,
                                               @RequestParam("longitude") Double longitude,
                                               @RequestParam("latitude") Double latitude) throws UserExistException {

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List<User> users = userService.search(currentUser, id, keyword, longitude, latitude);

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

}
