package com.coffy.controller;

import com.coffy.exception.EmailExistExeption;
import com.coffy.exception.UserExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by lulzimgashi on 04/03/2018.
 */

@RestControllerAdvice(annotations = {RestController.class})
public class ControllerAdvice {


    @ExceptionHandler(value = EmailExistExeption.class)
    public ResponseEntity<String> error(EmailExistExeption exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserExistException.class)
    public ResponseEntity<String> error(UserExistException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
