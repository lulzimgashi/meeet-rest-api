package com.coffy.configuration;

import com.coffy.model.Chat;
import com.coffy.model.User;
import com.coffy.repo.UserRepo;
import com.coffy.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.List;

/**
 * Created by lulzimgashi on 25/02/2018.
 */
@Component
public class SubscribedListener implements ApplicationListener<SessionSubscribeEvent> {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private MessageService messageService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void onApplicationEvent(SessionSubscribeEvent sessionSubscribeEvent) {

        String username = sessionSubscribeEvent.getUser().getName();
        User user = userRepo.findByEmail(username);

        List<Chat> messagesByUser = messageService.getMessagesByUser(user.getId());

        System.out.println("sending messages to : "+username);
        simpMessagingTemplate.convertAndSend("/users/" + user.getId(), messagesByUser);
    }
}
