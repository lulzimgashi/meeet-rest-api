package com.coffy.configuration;

import com.google.auth.ServiceAccountSigner;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.HttpMethod;
import com.google.cloud.storage.Storage;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.cloud.StorageClient;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by lulzimgashi on 12/02/2018.
 */
@Component
public class FirebaseConfig {

    public static ServiceAccountCredentials serviceAccountCredentials;

    public FirebaseConfig() {
        try {
            File file = new File(getClass().getClassLoader().getResource("key.json").getFile());
            FileInputStream key = new FileInputStream(file);

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredential(FirebaseCredentials.fromCertificate(key))
                    .setStorageBucket("market-web-78c47.appspot.com")
                    .build();
            FirebaseApp.initializeApp(options);
            serviceAccountCredentials = ServiceAccountCredentials.fromStream(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
