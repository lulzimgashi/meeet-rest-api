package com.coffy.service;

import com.coffy.configuration.FirebaseConfig;
import com.coffy.model.User;
import com.coffy.repo.UserRepo;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.firebase.cloud.StorageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by lulzimgashi on 12/02/2018.
 */
@Service
public class ImageService {

    @Autowired
    private UserRepo userRepo;

    public String upload(InputStream inputStream, String currentUser) {
        User user = userRepo.findByEmail(currentUser);

        Bucket bucket = StorageClient.getInstance().bucket();
        Blob obj = bucket.create(UUID.randomUUID().toString() + ".png", inputStream, "image/png");
        URL url = obj.signUrl(2000, TimeUnit.DAYS, Storage.SignUrlOption.signWith(FirebaseConfig.serviceAccountCredentials));

        user.setImageUrl(url.toString());
        userRepo.save(user);

        return url.toString();
    }
}
