package com.coffy.service;

import com.coffy.db.Queries;
import com.coffy.exception.EmailExistExeption;
import com.coffy.exception.UserExistException;
import com.coffy.model.Interes;
import com.coffy.model.User;
import com.coffy.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private InteresService interesService;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional
    public User addUser(User user) throws UserExistException {
        if (userRepo.findByEmail(user.getEmail()) != null) {
            throw new UserExistException("This email is already register");
        } else {
            user.setUsername(user.getEmail());
            user.setPassword(new StandardPasswordEncoder().encode(user.getPassword()));
            user.setEnabled(true);
            user.setStatus("Available");
            user.setInteresList(new ArrayList<>());
            return userRepo.save(user);
        }
    }

    public User updateUser(User user) throws EmailExistExeption {

        User dbUser = userRepo.findOne(user.getId());

        if (userRepo.checkIfExist(dbUser.getEmail(), user.getId())) {
            throw new EmailExistExeption("This email is already on use");
        } else {
            user.setUsername(user.getEmail());
            user.setPassword(dbUser.getPassword());
            user.setEnabled(true);
            return userRepo.save(user);
        }
    }

    public String changePassword(String email, String value) {
        User user = userRepo.findByEmail(email);

        user.setPassword(new StandardPasswordEncoder().encode(value));
        userRepo.save(user);
        return "PASSWORD_CHANGE";
    }

    public User getLoggedUser(String email) {
        User user = userRepo.findByEmail(email);
        user.setInteresList(interesService.getAllByUser(user.getId()));
        return user;
    }

    public List<User> search(String currentUser, String id, String keyword, Double longitude, Double latitude) {

        Map<String, Object> params = new HashMap<>();
        params.put("keyword", keyword);
        params.put("lat", latitude);
        params.put("lon", longitude);
        params.put("id", id);

        return namedParameterJdbcTemplate.query(Queries.USERS_BY_KEYWORD, params, resultSet -> {

            List<User> users = new ArrayList<>();

            String last = "";
            while (resultSet.next()) {
                //add users
                if (!resultSet.getString("u.id").equals(last)) {
                    User user = new User();
                    user.setId(resultSet.getString("u.id"));
                    user.setFirstName(resultSet.getString("u.first_name"));
                    user.setLastName(resultSet.getString("u.last_name"));
                    user.setEmail(resultSet.getString("u.email"));
                    user.setLocation(resultSet.getString("u.location"));
                    user.setLatitude(resultSet.getDouble("u.latitude"));
                    user.setLongitude(resultSet.getDouble("u.longitude"));
                    user.setImageUrl(resultSet.getString("u.image_url"));
                    user.setStatus(resultSet.getString("u.status"));
                    user.setInteresList(new ArrayList<>());
                    users.add(user);
                }

                //add interests
                Interes interes = new Interes();
                interes.setId(resultSet.getString("i.id"));
                interes.setName(resultSet.getString("i.name"));
                interes.setUserId(resultSet.getString("i.user_id"));

                users.get(users.size() - 1).getInteresList().add(interes);

                last = resultSet.getString("u.id");
            }

            return users;
        });
    }
}
