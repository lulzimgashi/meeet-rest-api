package com.coffy.service;

import com.coffy.db.Queries;
import com.coffy.model.Chat;
import com.coffy.model.ChatUser;
import com.coffy.model.Message;
import com.coffy.model.User;
import com.coffy.repo.ChatRepo;
import com.coffy.repo.ChatUserRepo;
import com.coffy.repo.MessageRepo;
import com.coffy.repo.UserRepo;
import com.google.api.client.util.ArrayMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 17/02/2018.
 */
@Service
public class MessageService {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private ChatRepo chatRepo;

    @Autowired
    private ChatUserRepo chatUserRepo;

    @Autowired
    private MessageRepo messageRepo;

    @Autowired
    private UserRepo userRepo;

    public List<Chat> getMessagesByUser(String id) {

        Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        return namedParameterJdbcTemplate.query(Queries.CHATS_BY_USER, params, resultSet -> {

            List<Chat> chats = new ArrayList<>();

            String last = "0";
            while (resultSet.next()) {
                if (!resultSet.getString("m.chat_id").equals(last)) {
                    Chat chat = new Chat();
                    chat.setId(resultSet.getString("m.chat_id"));

                    User user = new User();
                    user.setId(resultSet.getString("u.id"));
                    user.setFirstName(resultSet.getString("u.first_name"));
                    user.setLastName(resultSet.getString("u.last_name"));
                    user.setLocation(resultSet.getString("u.location"));
                    user.setEmail(resultSet.getString("u.email"));
                    user.setImageUrl(resultSet.getString("u.image_url"));
                    user.setStatus(resultSet.getString("u.status"));

                    chat.setUser(user);
                    chat.setMessages(new ArrayList<>());
                    chats.add(chat);
                }

                Message message = new Message();
                message.setId(resultSet.getString("m.id"));
                message.setChatId(chats.get(chats.size() - 1).getId());
                message.setDate(resultSet.getString("m.date"));
                message.setUserId(resultSet.getString("m.user_id"));
                message.setMessage(resultSet.getString("m.message"));
                chats.get(chats.size() - 1).getMessages().add(message);

                last = resultSet.getString("m.chat_id");
            }

            return chats;
        });
    }

    public void sendMessage(String chatId, Message message) {
        if (!chatRepo.exists(chatId)) {
            Chat chat = new Chat();
            chat.setId(chatId);
            chat.setDate(message.getDate());
            chatRepo.save(chat);

            ChatUser chatUser = new ChatUser();
            chatUser.setChatId(chatId);
            chatUser.setUserId(message.getUserId());
            chatUserRepo.save(chatUser);

            ChatUser chatUser2 = new ChatUser();
            chatUser2.setChatId(chatId);
            chatUser2.setUserId(message.getTo());
            chatUserRepo.save(chatUser2);

            User user = userRepo.findOne(message.getUserId());
            chat.setUser(user);

            List<Message> messages=new ArrayList<>();
            messages.add(message);

            chat.setMessages(messages);

            simpMessagingTemplate.convertAndSend("/users/" + message.getTo(), chat);
        } else {
            simpMessagingTemplate.convertAndSend("/users/" + message.getTo(), message);
        }
        messageRepo.save(message);
    }
}
