package com.coffy.service;

import com.coffy.model.Interes;
import com.coffy.repo.InteresRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
@Service
public class InteresService {

    @Autowired
    private InteresRepo interesRepo;

    public List<Interes> getAllByUser(String id) {
        return interesRepo.findAllByUserId(id);
    }

    public Interes addInteres(Interes interes) {
        return interesRepo.save(interes);
    }

    public String deleteInteres(String id) {
        interesRepo.delete(id);
        return "INTERES_DELETED";
    }
}
