package com.coffy.exception;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
public class EmailExistExeption extends Throwable {

    public EmailExistExeption(String message) {
        super(message);
    }
}
