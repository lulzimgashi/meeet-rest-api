package com.coffy.exception;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
public class UserExistException extends Throwable {

    public UserExistException(String message) {
        super(message);
    }
}
