package com.coffy.repo;

import com.coffy.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 17/02/2018.
 */
@Repository
public interface MessageRepo extends JpaRepository<Message,String> {
}
