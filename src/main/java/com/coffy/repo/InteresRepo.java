package com.coffy.repo;

import com.coffy.model.Interes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
@Repository
public interface InteresRepo extends JpaRepository<Interes, String> {

    List<Interes> findAllByUserId(String id);
}
