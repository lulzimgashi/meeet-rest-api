package com.coffy.repo;

import com.coffy.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
@Repository
public interface UserRepo extends JpaRepository<User, String> {

    User findByEmail(String email);

    @Query("select CASE WHEN COUNT(u) > 0 THEN true ELSE false END from User u where u.email=?1 AND u.id<>?2")
    boolean checkIfExist(String email, String id);
}
