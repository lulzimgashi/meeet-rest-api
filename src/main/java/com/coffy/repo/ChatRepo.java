package com.coffy.repo;

import com.coffy.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 17/02/2018.
 */
@Repository
public interface ChatRepo extends JpaRepository<Chat, String> {
}
