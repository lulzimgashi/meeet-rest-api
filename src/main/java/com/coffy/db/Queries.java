package com.coffy.db;

/**
 * Created by lulzimgashi on 11/02/2018.
 */
public class Queries {
    public static final String USERS_BY_KEYWORD = "SELECT lat_lng_distance(u.latitude, u.longitude, :lat, :lon) AS distance, u.*, i.* FROM users u INNER JOIN interests i ON u.id = i.user_id WHERE lat_lng_distance(u.latitude, u.longitude, :lat, :lon) < 10 AND(IF(length(:keyword) = 0, TRUE, lower(i.name) LIKE lower(CONCAT_WS(:keyword, '%', '%'))) OR lower(CONCAT_WS(' ', u.first_name, u.last_name)) LIKE lower(CONCAT_WS(:keyword, '%', '%'))) AND u.id != :id ORDER BY distance, u.status LIMIT 100;";

    public static final String CHATS_BY_USER = "SELECT m.*, u.id, u.first_name, u.last_name, u.location, u.email, u.image_url, u.status FROM messages m INNER JOIN(SELECT * FROM user_chat WHERE user_id = :id) uch ON uch.chat_id = m.chat_id INNER JOIN user_chat uch2 ON (uch.chat_id = uch2.chat_id AND uch2.user_id != :id) INNER JOIN users u ON uch2.user_id = u.id INNER JOIN chats ch ON uch2.chat_id=ch.id ORDER BY ch.date, m.chat_id, date;";
}
