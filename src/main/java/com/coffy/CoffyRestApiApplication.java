package com.coffy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoffyRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoffyRestApiApplication.class, args);
	}
}
